#!/usr/bin/env bash

if [ ! -d "node_modules/react-native/third-party" ]; then
  cd node_modules/react-native ; ./scripts/ios-install-third-party.sh ; cd ../../
  cd node_modules/react-native/third-party/glog-* ; ../../scripts/ios-configure-glog.sh
fi
