import { YellowBox } from 'react-native';
import { registerScreens } from './Screens';
import { Navigation } from 'react-native-navigation';
import theme from './Theme';

YellowBox.ignoreWarnings(['Require cycle:']);
registerScreens();

const children = [
  {
    stack: {
      children: [{ component: { name: 'labbe.nicolas.HomeScreen', passProps: {} } }],
      options: {
        bottomTab: {
          iconInsets: { top: 5, left: 0, bottom: 0, right: 0 },
          testID: 'homeTabsTestID',
          icon: require('./assets/icon/beer_off.png'),
          selectedIcon: require('./assets/icon/beer_on.png')
        }
      }
    }
  },
  {
    stack: {
      children: [{ component: { name: 'labbe.nicolas.ScanScreen', passProps: {} } }],
      options: {
        bottomTab: {
          iconInsets: { top: 5, left: 0, bottom: 0, right: 0 },
          testID: 'scanTabsTestID',
          icon: require('./assets/icon/barcode_off.png'),
          selectedIcon: require('./assets/icon/barcode_on.png')
        }
      }
    },
  },
  {
    stack: {
      children: [{ component: { name: 'labbe.nicolas.HistoryScreen', passProps: {} } }],
      options: {
        bottomTab: {
          iconInsets: { top: 5, left: 0, bottom: 0, right: 0 },
          testID: 'historyTabsTestID',
          icon: require('./assets/icon/history_off.png'),
          selectedIcon: require('./assets/icon/history_on.png')
        }
      }
    },
  }
];

Navigation.events().registerAppLaunchedListener(() => {

  Navigation.setDefaultOptions({
    topBar: {
      // hideOnScroll: true,
      buttonColor: '#b3b3b3',
      backButton: {
        color: '#b3b3b3',
        icon: require('./assets/icon/left-arrow.png'),
        visible: true
      },
      title: {
        padding: 20,
        fontSize: 20,
        alignment: 'fill',
        fontFamily: theme.font.regular,
        fontWeight: '900'
      }
    }
  });

  Navigation.setRoot({
    root: {
      bottomTabs: {
        iconInsets: { top: 5, left: 0, bottom: 0, right: 0 },
        visible: true,
        animate: false, // Controls whether BottomTabs visibility changes should be animated
        iconInsets: { top: 5, left: 0, bottom: 0, right: 0 },
        currentTabIndex: 0,
        currentTabId: 'currentTabId',
        testID: 'bottomTabsTestID',
        iconInsets: { top: 5, left: 0, bottom: 0, right: 0 },
        drawBehind: false,
        backgroundColor: 'white',
        children: children
      }
    }
  });
});

// Navigation.events().registerAppLaunchedListener(() => {
//   Navigation.setRoot({
//     root: {
//       component: {
//         name: 'navigation.playground.WelcomeScreen'
//       }
//     }
//   });
// });


// // start the app
// Navigation.startTabBasedApp({
//   tabs: tabs,
//   disableIconTint: true,
//   disableSelectedIconTint: true,
//   tabsStyle: {
//     tabBarButtonColor: null, // change the color of the tab icons and text (also unselected)
//     tabBarSelectedButtonColor: null, // change the color of the selected tab icon and text (only selected)
//     tabBarBackgroundColor: 'white' // change the background color of the tab bar
//   },
//   animationType: Platform.OS === 'ios' ? 'slide-down' : 'fade',
// });
