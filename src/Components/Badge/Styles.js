import styled from 'styled-components';

export const Circle = styled.View`
  width: ${({ size }) => `${size}px`};
  height: ${({ size }) => `${size}px`};
  border-radius: ${({ size }) => `${size/2}px`};
  margin: 0 5px 0 0;
  background: ${({ color }) => color};
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  right: 10;
  top: 10;
`;

export const Score = styled.Text`
  color: white;
  font-size: 18;
  font-family: ${({ theme }) => theme.font.light};
`;
