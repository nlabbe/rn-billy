import React from 'react';
import {
  Circle,
  Score
} from './Styles';

const scoreToColor = (score) => {
  if(!score) return '#d3d3d3';
  if(score < 25) return '#ff3d00';
  if(score < 50) return '#1de9b6';
  if(score < 75) return '#ff9100';
  return '#00e676';
}

export default Badge = props => (
  <Circle
    size={60}
    color={scoreToColor(props.score)}>
    <Score>{props.score}</Score>
  </Circle>
);
