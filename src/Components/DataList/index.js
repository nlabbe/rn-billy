import React from 'react';
import {
  ListView,
  View,
  TouchableOpacity
} from '@shoutem/ui';
import Preview from '../Preview';
import product from '../../Utils/product';

export default List = props => (
  <View
    style={{
      pading: 0,
      height: '100%'
    }}>
    <ListView
      style={{
        pading: 0,
        height: '100%'
      }}
      data={props.data}
      renderRow={(item, s, i) => item.barcode
        ? <TouchableOpacity
            style={{
              backgroundColor: '#FFF'
            }}
            onPress={() => props.onPress && props.onPress(item.barcode)}>
            <Preview {...product.header(item).header} />
          </TouchableOpacity>
        : null}
    />
  </View>
);
