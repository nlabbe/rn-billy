import styled from 'styled-components';

export const View = styled.View`
  flex-direction: row;
  padding: ${({ grade }) => grade ? '5px 10px' : '5px'};
  align-items: center;
  justify-content: center;
  background: #fff;
  top: ${({ grade }) => grade ? '-60px' : '-40px'};
  align-self: center;
  border-radius: ${({ grade }) => grade ? '30px' : '15px'};
  position: absolute;
`;

export const Background = styled.View`
  ${({ left }) => left && `
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  `}
  ${({ right }) => right && `
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  `}
  ${({ active }) => active && `
  border-top-right-radius: 20px;
  border-bottom-right-radius: 20px;
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  `}
  padding: 5px 10px;
  ${({ active }) => active && 'border-width: 4;'}
  border-color: #fff;
  background-color: ${({ color }) => color};
`;

export const Score = styled.Text`
  color: #fff;
  font-family: ${({ active, theme }) => active ? theme.font.bold : theme.font.light};
  font-size: ${({ active }) => active ? '38px' : '18px'};
`;
