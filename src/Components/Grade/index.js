import React from 'react';
import {
  View,
  Score,
  Background
} from './Styles';

const gradeToColor = (grade) => {
  if(!grade) return '#d3d3d3';
  if(grade === 'a') return '#1de9b6';
  if(grade === 'b') return '#00e676';
  if(grade === 'c') return '#fdd835';
  if(grade === 'd') return '#ff9100';
  return '#ff3d00';
};

export default Grade = props => <View
  grade={props.grade}>
  {['a', 'b', 'c', 'd', 'e'].map((i) => <Background
    color={gradeToColor(i)}
    active={i === props.grade}
    key={i}
    left={i === 'a'}
    right={i === 'e'}>
    <Score
      active={i === props.grade}>
      {i.toUpperCase()}
    </Score>
  </Background>)}
</View>;
