import React from 'react';
import styled from 'styled-components';
import * as Shoutem from '@shoutem/ui';
import theme from '../../Theme';

export const ImgWrapper = styled.View`
  width: 100%;
  height: 200px;
`;

export const Img = styled.Image`
  width: 100%;
  height: 100%;
  z-index: 1;
`;

export const ImgOverlay = styled.View`
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.4);
  z-index: 2;
  position: absolute;
`;

export const Wrapper = (props) => <Shoutem.View
  styleName="vertical"
  style={{
    backgroundColor: '#FFF',
    borderBottomColor: '#f5f5f5',
    borderBottomWidth: 1
  }}
  {...props} />;

export const Title = (props) => <Shoutem.Title
  style={{
    color: '#878787',
    lineHeight: 32,
    fontSize: 30,
    fontFamily: theme.font.bold,
    backgroundColor: props.children ? 'transparent' : '#f5f5f5'
  }}
  {...props} />;

export const Subtitle = (props) => <Shoutem.Subtitle
  style={{
    color: '#b3b3b3',
    fontFamily: theme.font.bold,
    backgroundColor: props.children ? 'transparent' : '#f5f5f5'
  }}
  {...props} />;
