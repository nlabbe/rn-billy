import React from 'react';
import {
  View
} from '@shoutem/ui';
import {
  Img,
  Title,
  Subtitle,
  ImgWrapper,
  ImgOverlay,
  Wrapper
} from './Styles';
import Badge from '../Badge';
import Grade from '../Grade';

const scoreToText = (score) => {
  if(!score) return 'Unknow';
  if(score < 25) return 'Bad';
  if(score < 50) return 'Poor';
  if(score < 75) return 'Good';
  return 'Excellent';
}

export default render = ({ img, title, brand, score, grade }) => <Wrapper>
  <ImgWrapper>
    {img && <Img
      source={{uri: img.small}} />}
      <ImgOverlay />
  </ImgWrapper>
  <View
    style={{
      alignItems: 'flex-start',
      padding: 20,
      marginTop: 25
    }}
    styleName="vertical"
    grade={grade}>
    <Grade grade={grade} />
    <Title>{title}</Title>
    <Subtitle>{brand}</Subtitle>
  </View>
  <Badge
    score={score} />
</Wrapper>;
