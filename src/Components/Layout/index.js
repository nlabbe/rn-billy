import React from 'react';
import styled, { ThemeProvider } from 'styled-components';
import theme from '../../Theme';

const StyledView = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

export default ({ children }) => <ThemeProvider theme={theme}>
  <StyledView>
    {children}
  </StyledView>
</ThemeProvider>;
