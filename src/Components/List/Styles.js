import React from 'react';
import styled from 'styled-components';
import { Image, Row, View } from '@shoutem/ui';

export const TitleView = styled.View`
  background-color: #fff;
  border-bottom-color: #f5f5f5;
  border-bottom-width: 1;
`;

export const Item = styled.View`
  background-color: #fff;
`;

export const StyledImage = (props) => <Image
  style={{
    width: 100,
    height: 100,
    borderRadius: 50
  }}
  {...props} />;
export const Left = styled.View`
  align-items: flex-start;
`;

export const Right = styled.View`
  flex: 1;
  align-items: flex-end;
`;

export const StyledHeader = (props) => <Row
  class="horizontal"
  {...props} />;

export const Horizontal = (props) => <Row
  style={{
    backgroundColor: 'transparent'
  }}
  {...props} />;

export const Percentage = (props) => props.percent && <View
  style={{
    width: '90%',
    height: 2,
    left: '5%',
    position: 'absolute',
    bottom: 0
  }} >
    <View
    style={{
      width: `${props.percent < 100 ? props.percent : 100}%`,
      height: 2,
      backgroundColor: props.color
    }} />
  </View>;
