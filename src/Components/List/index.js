import React from 'react';
import {
  ListView,
  View,
  Title,
  Image,
  Subtitle,
  Text,
  Caption
} from '@shoutem/ui';
import {
  Item,
  Left,
  Right,
  Horizontal,
  TitleView,
  Percentage
} from './Styles';
import Header from '../Header';
import Nova from '../Nova';

const _object = (values) => values && <Horizontal>
  {values.icon && <Image source={values.icon} /> }
  <Left>
    <Subtitle>{values.left}</Subtitle>
  </Left>
  <Right>
    <Caption>{values.right}</Caption>
  </Right>
  {typeof values.percent !== 'undefined'
    && values.percent !== null
    && values.percent > 0 && <Percentage
    percent={values.percent}
    color={values.color}
    /> }
</Horizontal>;

const _values = (value) => {
  try {
    if(typeof value === 'object') {
      return _object(value);
    }
    return <Horizontal>
      <Text>{value}</Text>
    </Horizontal>;
  }catch(e) {
    console.log('err', e, value);
    return <React.Fragment />;
  }
}

const _renderItem = (item, s, i) => {
  const { header, value, title, nova, grade } = item;

  const novaRender = nova ? <Nova nova={nova} /> : null;
  const headerRender = header ? <Header {...header} /> : null;
  const valueRender = value ? _values(value) : null;
  const titleRender = title ? _values(title) : null;

  return <React.Fragment>
    {headerRender && headerRender}
    {novaRender && novaRender}
    {valueRender && <Item key={i}>{valueRender}</Item>}
    {titleRender && <TitleView key={i}>{titleRender}</TitleView>}
  </React.Fragment>;
};

export default List = props => (
  <View>
    <ListView
      style={{
        padding: 0
      }}
      data={props.data}
      renderRow={_renderItem}
    />
  </View>
);
