import styled from 'styled-components';

export const View = styled.View`
  flex-direction: row;
  padding: 10px;
  align-items: center;
  justify-content: center;
  background: #fff;
`;

export const Score = styled.Text`
  color: ${({ color }) => color};
  font-family: ${({ active, theme }) => active ? theme.font.bold : theme.font.light};
  font-size: ${({ active }) => active ? '38px' : '18px'};
  margin-left: 10px;
`;
