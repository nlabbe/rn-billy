import React from 'react';
import {
  View,
  Score
} from './Styles';

const novaToColor = (nova) => {
  if(!nova) return '#d3d3d3';
  if(nova === 1) return '#1de9b6';
  if(nova === 2) return '#00e676';
  if(nova === 3) return '#ff9100';
  return '#ff3d00';
}

export default Nova = props => (
  <View>
    {[1, 2, 3, 4].map((i) => <Score
      key={i}
      color={novaToColor(i)}
      active={i === props.nova}>
      {i}
    </Score>)}
  </View>
);
