import React from 'react';
import styled from 'styled-components';
import * as Shoutem from '@shoutem/ui';
import theme from '../../Theme';

const border = 0;

export const ColorScore = styled.View`
  right: 6;
  top: 2;
  width: 8;
  height: 8;
  border-radius: 4;
  background-color: ${({ color }) => color ? color : '#f5f5f5'};
  position: absolute;
`;

export const ImgWrapper = styled.View`
  width: ${50 + border};
  height: ${50 + border};
  border-radius: ${(50 + border) / 2};
  background-color: #f5f5f5;
  margin-right: 10px;
`;

export const Img = (props) => <Shoutem.Image
  style={{
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    left: border / 2,
    top: border / 2
  }}
  {...props} />;

export const Wrapper = (props) => <Shoutem.Row
  style={{
    padding: 0,
    marginLeft: 10,
    marginRight: 10
  }}
  {...props} />;

export const Inner = styled.View`
  padding: 20px;
  border-bottom-color: #f5f5f5;
  border-bottom-width: 1;
  flex-direction: row;
`;

export const Title = (props) => <Shoutem.Title
  style={{
    backgroundColor: props.children ? '#FFFFFF' : '#f5f5f5',
    fontSize: 13,
    lineHeight: 15,
    marginTop: 5,
    fontFamily: theme.font.bold
  }}
  {...props} />;

export const Subtitle = (props) => <Shoutem.Subtitle
  style={{
    backgroundColor: props.children ? '#FFFFFF' : '#f5f5f5',
    fontSize: 12,
    lineHeight: 14,
    marginTop: 5,
    flex: 1,
    fontFamily: theme.font.regular,
    color: '#b3b3b3'
  }}
  {...props} />;

export const Caption = (props) => <Shoutem.Caption
  style={{
    marginTop: 5,
    flex: 1,
    backgroundColor: props.children ? '#FFFFFF' : '#f5f5f5',
    fontSize: 12,
    lineHeight: 14,
    fontWeight: '100',
    fontFamily: theme.font.regular,
    color: '#b3b3b3'
  }}
  {...props} />;
