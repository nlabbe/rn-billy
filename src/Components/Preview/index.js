import React from 'react';
import {
  View
} from '@shoutem/ui';
import {
  Img,
  Title,
  Subtitle,
  Caption,
  ImgWrapper,
  Wrapper,
  Inner,
  ColorScore
} from './Styles';

const scoreToText = (score) => {
  if(!score) return 'Unknow';
  if(score < 25) return 'Bad';
  if(score < 50) return 'Poor';
  if(score < 75) return 'Good';
  return 'Excellent';
}

const scoreToColor = (score) => {
  if(!score) return '#d3d3d3';
  if(score < 25) return '#ff3d00';
  if(score < 50) return '#1de9b6';
  if(score < 75) return '#ff9100';
  return '#00e676';
}

export default render = ({ img, title, brand, score }) => <Wrapper>
  <Inner>
    <ImgWrapper>
      {img && <Img
        styleName="medium-avatar"
        source={{uri: img.small}} />}
      {score && <ColorScore
          color={scoreToColor(score)} />}
    </ImgWrapper>
    <View styleName="horizontal">
      <View
      style={{
        flex: 1
      }}
      styleName="vertical">
        <Title>{title}</Title>
        <Subtitle>{brand}</Subtitle>
      </View>
      <View
        style={{ alignItems: 'center' }}
        styleName="horizontal">
        <View styleName="vertical">
          <Caption>{scoreToText(score)}</Caption>
        </View>
      </View>
    </View>
  </Inner>
</Wrapper>;
