import styled from 'styled-components';

export const Container = styled.View`
  flex: 1;
  align-self: stretch;
  background-color: #F5FCFF;
  flex-direction: column;
`;
