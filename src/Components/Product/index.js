import React, { PureComponent } from 'react';
import {
  Container
} from './Styles';
import List from '../List';
import product from '../../Utils/product';
import * as Data from '../../Data';

export default class Product extends PureComponent {
  constructor(props) {
    super(props);
  }

  state = {
    barcode: null,
    isLoading: false,
    error: null,
    data: product.toList({}),
    search: async (code) => {
      const { result, error } = await Data.find(code);

      const list = product.toList(result);

      this.setState({
        barcode: code,
        isLoading: false,
        error: error,
        data: list
      }, function(){

      });
    }
  }

  static getDerivedStateFromProps(props, state) {
    if(props.barcode !== state.barcode) {
      state.search(props.barcode);

      return {
        barcode: props.barcode,
        data: product.toList({}),
        error: null,
        isLoading: true
      };
    }

    return null;
  }

  render() {
    const { data } = this.state;

    return (
      <Container>
        <List data={data} />
      </Container>
    );
  }
}
