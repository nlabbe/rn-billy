import styled from 'styled-components';
import { RNCamera } from 'react-native-camera';
// import { Dimensions } from 'react-native'

export const Container = styled.View`
  flex: 1;
  align-self: stretch;
  justify-content: center;
  align-items: center;
  background-color: #F5FCFF;
  flex-direction: column;
`;

export const Camera = styled(RNCamera)`
  flex: 1;
  align-self: stretch;
`;

export const Flash = styled.TouchableOpacity`
  position: absolute;
  align-items: center;
  justify-content: center;
  top: 10;
  bottom: 10;
  height: 50;
  width: 50;
  left: 20;
`;
