import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { RNCamera } from 'react-native-camera';
import {
  CameraRoll,
  Platform,
  Alert
} from 'react-native';
import {
  Container,
  Camera
} from './Styles';
import * as Data from '../../Data';
import product from '../../Utils/product';

export default class Example extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    isLoading: false,
    search: (code) => {
      if(!this.state.isLoading) {
        this.setState({ isLoading: true });

        fetch(`https://world.openfoodfacts.org/api/v0/produit/${code}.json`)
          .then(async (response) => response.json())
          .then(async (json) => {
            const result = product.convert(json);

            await Data.save(code, result);

            Navigation.push(this.props.componentId, {
              component: {
                name: 'labbe.nicolas.DetailScreen',
                passProps: {
                  barcode: code
                },
                options: {
                  topBar: {
                    title: {
                      text: 'Detail'
                    }
                  }
                }
              }
            });

            this.setState({ isLoading: false });
          });
      }
    }
  }

  static getDerivedStateFromProps(props, state) {
    if(typeof props.barcode !== 'undefined'
      && props.barcode !== null
      && props.barcode !== state.barcode
      && !state.isLoading) state.search(props.barcode)
    return null;
  }

  render() {
    const { isLoading, search } = this.state;

    return (
      <Container>
        {!isLoading && <Camera
            ref={ref => this.camera = ref }
            onBarCodeRead={(e) => {
              if(!isLoading) search(e.data);
            }}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.off}
            permissionDialogTitle={'Permission to use camera'}
            permissionDialogMessage={'We need your permission to use your camera phone'}
          />}
      </Container>
    );
  }
}
