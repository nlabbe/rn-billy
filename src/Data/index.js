import { AsyncStorage } from "react-native";

export const save = async (id, json) => {
  let result = null;
  let error = null;
  try {
    result = await AsyncStorage.setItem(id, JSON.stringify(json));
  } catch (error) {
    error = error;
  }

  return { result, error };
};

export const find = async (id) => {
  let result = null;
  let error = null;
  try {
    result = JSON.parse(await AsyncStorage.getItem(id));
  } catch (error) {
    error = error;
  }

  return { result, error };
};

export const all = async () => {
  let result = null;
  let error = null;
  try {
    const keys = await AsyncStorage.getAllKeys();
    result = await Promise.all(keys.map( async (key) => {
      const { result, error } = await find(key)
      return result;
    } ));
  } catch (error) {
    error = error;
  }

  return { result, error };
};
