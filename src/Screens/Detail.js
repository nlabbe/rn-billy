import React from 'react';
import Product from '../Components/Product';
import Layout from '../Components/Layout';

export default class Detail extends React.Component {
  static options(passProps) {
    return {
      topBar: {
        title: {
          text: 'Detail'
        }
      }
    };
  }

  render() {
    const { barcode } = this.props;

    return (
      <Layout>
        <Product barcode={barcode} />
      </Layout>
    );
  }
}
