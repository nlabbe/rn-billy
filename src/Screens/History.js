import React from 'react';
import { Navigation } from 'react-native-navigation';
import * as Data from '../Data';
import DataList from '../Components/DataList';
import Layout from '../Components/Layout';

export default class History extends React.Component {
  static options(passProps) {
    return {
      topBar: {
        title: {
          text: 'History'
        }
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  state = {
    list: []
  }

  async componentDidAppear() {
    const { result, error } = await Data.all();

    this.setState({
      list: result
    });
  }

  render() {
    const { list } = this.state;
    const { componentId } = this.props;

    return (
      <Layout>
        <DataList
          data={list}
          onPress={(code) => Navigation.push(componentId, {
              component: {
                name: 'labbe.nicolas.DetailScreen',
                passProps: {
                  barcode: code
                },
                options: {
                  topBar: {
                    title: {
                      text: 'Detail'
                    }
                  }
                }
              }
            })} />
      </Layout>
    );
  }
}
