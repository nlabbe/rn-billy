import React from 'react';
import { Text } from 'react-native';
import Layout from '../Components/Layout';

export default class Home extends React.Component {
  static options(passProps) {
    return {
      topBar: {
        title: {
          text: 'Home'
        }
      }
    };
  }

  render() {
    return (
      <Layout>
        <Text>Home</Text>
      </Layout>
    );
  }
}
