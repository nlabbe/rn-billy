import React from 'react';
import { Navigation } from 'react-native-navigation';
import Scanner from '../Components/Scanner';
import Layout from '../Components/Layout';

export default class Scan extends React.PureComponent {
  static options(passProps) {
    return {
      topBar: {
        visible: false,
        title: {
          text: 'Home'
        }
      },
      bottomTabs: {
        translucent: true,
        iconColor: 'white',
        blur: false
      },
      bottomTab: {
        disableIconTint: false, //set true if you want to disable the icon tinting
        disableSelectedIconTint: false,
        selectedIconColor: 'white'
      }
    };
  }

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  state = {
    barcode: null
  }

  componentDidAppear() {
    this.setState({ barcode: null });

    if(process && process.env && process.env.NODE_ENV === 'development'
      && this.state.barcode === null) {
      const fake = [
        '3366321052331', // st hubert
        '3259011046300', // tofu
        '3017620425400', // nutella
        '3068320099613', // badoit
        '5410228218067' // leffe
      ];

      setTimeout(() => this.setState({ barcode: fake[Math.floor(Math.random()*fake.length)] }), 1000);
    }
  }

  componentDidDisappear() {
    this.setState({ barcode: null });
  }

  render() {
    const { barcode } = this.state;
    const { componentId } = this.props;

    return (
      <Layout>
        <Scanner componentId={componentId} barcode={barcode} />
      </Layout>
    );
  }
}
