import { Navigation, ScreenVisibilityListener } from 'react-native-navigation';

import Home from './Home';
import Scan from './Scan';
import History from './History';
import Detail from './Detail';

export function registerScreens() {
  Navigation.registerComponent('labbe.nicolas.HomeScreen', () => Home);
  Navigation.registerComponent('labbe.nicolas.ScanScreen', () => Scan);
  Navigation.registerComponent('labbe.nicolas.DetailScreen', () => Detail);
  Navigation.registerComponent('labbe.nicolas.HistoryScreen', () => History);
}

export function registerScreenVisibilityListener() {
  new ScreenVisibilityListener({
    willAppear: ({screen}) => console.log(`Displaying screen ${screen}`),
    didAppear: ({screen, startTime, endTime, commandType}) => console.log('screenVisibility', `Screen ${screen} displayed in ${endTime - startTime} millis [${commandType}]`),
    willDisappear: ({screen}) => console.log(`Screen will disappear ${screen}`),
    didDisappear: ({screen}) => console.log(`Screen disappeared ${screen}`)
  }).register();
}
