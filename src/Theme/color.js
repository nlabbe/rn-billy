const unknow = '#d3d3d3';
const excelent = '#1de9b6';
const good = '#00e676';
const medium = '#fdd835';
const poor = '#ff9100';
const bad = '#ff3d00';

export default {
  unknow,
  excelent,
  good,
  medium,
  poor,
  bad
}
