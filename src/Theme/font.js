const bold = 'Avenir-Black';
const regular = 'Avenir';
const light = 'Avenir-light';

export default {
  bold,
  regular,
  light
}
