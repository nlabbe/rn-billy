const _convertObject = (name, value, unit, icon) => ({ name: name, value: value, unit: unit, icon: icon });

const notNull = (obj) => {
  if(Array.isArray(obj) && obj.length > 0) {
    return obj;
  }else if(typeof obj !== 'undefined' && obj !== null && obj !== '') {
    return obj;
  }
  return null;
}

const _getNutriments = (nutriments) => {
  const result = [];
  if(nutriments.proteins) result.push(_convertObject('proteins', nutriments.proteins, nutriments.proteins_unit ));
  if(nutriments['saturated-fat']) result.push(_convertObject('saturated-fat', nutriments['saturated-fat'], nutriments['saturated-fat_unit'] ));
  if(nutriments.fat) result.push(_convertObject('fat', nutriments.fat, nutriments.fat_unit ));
  if(nutriments.sugars) result.push(_convertObject('sugars', nutriments.sugars, nutriments.sugars_unit ));
  if(nutriments.salt) result.push(_convertObject('salt', nutriments.salt, nutriments.salt_unit ));
  if(nutriments.carbohydrates) result.push(_convertObject('carbohydrates', nutriments.carbohydrates, nutriments.carbohydrates_unit ));
  if(nutriments.sulfates) result.push(_convertObject('sulfates', nutriments.sulfates, nutriments.sulfates_unit ));
  if(nutriments.potassium) result.push(_convertObject('potassium', nutriments.potassium, nutriments.potassium_unit ));
  if(nutriments.silica) result.push(_convertObject('silica', nutriments.silica, nutriments.silica_unit ));
  if(nutriments.chloride) result.push(_convertObject('chloride', nutriments.chloride, nutriments.chloride_unit ));
  if(nutriments.fiber) result.push(_convertObject('fiber', nutriments.fiber, nutriments.fiber_unit ));
  if(nutriments.bicarbonate) result.push(_convertObject('bicarbonate', nutriments.bicarbonate, nutriments.bicarbonate_unit ));
  if(nutriments.magnesium) result.push(_convertObject('magnesium', nutriments.magnesium, nutriments.magnesium_unit ));
  if(nutriments.calcium) result.push(_convertObject('calcium', nutriments.calcium, nutriments.calcium_unit ));
  if(nutriments.chrome) result.push(_convertObject('chrome', nutriments.chrome, nutriments.chrome_unit ));
  if(nutriments.fluoride) result.push(_convertObject('fluoride', nutriments.fluoride, nutriments.fluoride_unit ));
  if(nutriments.sodium) result.push(_convertObject('sodium', nutriments.sodium, nutriments.sodium_unit ));
  return notNull(result);
}

const _getScore = (nutriments) => notNull(nutriments['nutrition-score-fr']);
const _getAllergens = (allergens) => {
  const result = [];
  allergens.toLowerCase().split(',').map((allergen) => {
    if(notNull(allergen)) result.push(allergen.trim());
  });
  return notNull(result);
};
const _getNutrientLevels = (nutrients) => notNull(nutrients && Object.keys(nutrients).map((key) => _convertObject(key, nutrients[key])));
const _getIngredients = (ingredients) => {
  const result = [];
  let other = '';
  notNull(ingredients.map((ingredient) => {
    if(ingredient.percent) {
      result.push(_convertObject(ingredient.text.toLowerCase(), parseFloat(ingredient.percent), '%'));
    }else {
      other += other === ''  ? ingredient.text.toLowerCase() : `, ${ingredient.text.toLowerCase()}`;
    }
  }));
  result.push(_convertObject(other));
  return result.sort((a, b) => a.value < b.value);
};

const _getAdditives = (additives) => {
  const result = [];
  additives.map((additive) => {
    const splitted = additive.split(':');
    if(splitted && splitted[1]) result.push(splitted[1]);
  });
  return notNull(result);
}

const _getImages = (images, original) => ({
  thumb: images && images.front && images.front.thumb && images.front.thumb.fr,
  small: images && images.front && images.front.small && images.front.small.fr,
  large: images && images.front && images.front.display && images.front.display.fr,
  original: original
})

const _getLabels = (labels) => {
  const result = [];
  labels.toLowerCase().split(',').map((label) => {
    if(notNull(label)) result.push(label.trim());
  });
  return notNull(result);
};

const _isBio = (labels) => {
  let result = false;
  labels.toLowerCase().split(',').map((label) => {
    if(label.indexOf('bio')) result = true;
  });
  return result;
};

const convert = (json) => {
  console.log('json', json);
  const product = {};

  product.img = _getImages(json.product.selected_images, json.product.image_url);
  product.title = json.product.generic_name || json.product.product_name;
  product.brand = json.product.brands;
  product.score = _getScore(json.product.nutriments);

  if(notNull(json.product.nutrition_grades)) product.grade = json.product.nutrition_grades;
  if(notNull(json.product.nova_group)) product.nova = parseInt(json.product.nova_group);
  if(json.product.nutriments && json.product.nutriments.energy) product.energy = _convertObject('Énergie', json.product.nutriments.energy, json.product.nutriments.energy_unit );
  product.nutriments = _getNutriments(json.product.nutriments);
  if(notNull(json.product.quantity)) product.quantity = json.product.quantity;
  if(notNull(json.product.unknown_ingredients_n)) product.ingredientsUnknown = json.product.unknown_ingredients_n;
  if(notNull(json.product.packaging_tags)) product.packaging = json.product.packaging_tags;
  if(notNull(json.product.code)) product.barcode = json.product.code;
  if(notNull(json.product._keywords)) product.keywords = json.product._keywords.join(', ');
  if(notNull(json.product.ingredients_from_palm_oil_tags)) product.palm = json.product.ingredients_from_palm_oil_tags.join(',');
  product.additives = _getAdditives(json.product.additives_tags).join(', ');
  product.allergens = _getAllergens(json.product.allergens_from_ingredients);
  if(notNull(json.product.labels)) product.labels = _getLabels(json.product.labels);
  product.bio = _isBio(json.product.labels);
  product.ingredient = _getIngredients(json.product.ingredients);
  product.nutrientLevels = _getNutrientLevels(json.product.nutrient_levels);

  return product;
}

const header = ({ img, title, brand, score, grade }) => ({
  header: {
    img: img,
    title: title,
    brand: brand,
    score: score,
    grade: grade
  }
});

const placeholder = {
  img: null,
  title: null,
  brand: null,
  score: null
};

getPercent = (title, value) => {
  switch(title) {
    case 'proteins':
      return value * 100 / 16;
    case 'salt':
      return value * 100 / 2.3;
    case 'fiber':
      return value * 100 / 7;
    case 'sodium':
      return value * 100 / 0;
    case 'fat':
      return value * 100 / 800;
    case 'saturated-fat':
      return value * 100 / 10;
    case 'sugars':
      return value * 100 / 45;
    case 'carbohydrates':
      return value * 100 / 100;
    default:
      return null;
  }
}

const disabled = '#d3d3d3';
const excellent = '#00e676';
const good = '#1de9b6';
const poor = '#ff9100';
const bad = '#ff3d00';

getColor = (title, percent) => {
  switch(title) {
    case 'proteins':
      return percent > 50 ? excellent : good;
    case 'salt':
      if(percent < 25) return excellent;
      if(percent < 50) return good;
      if(percent < 75) return bad;
      return poor;
    case 'fiber':
      return percent > 50 ? excellent : good;
    case 'sodium':
      return disabled;
    case 'fat':
      if(percent < 25) return excellent;
      if(percent < 50) return good;
      if(percent < 75) return bad;
      return poor;
    case 'saturated-fat':
      if(percent < 25) return excellent;
      if(percent < 50) return good;
      if(percent < 75) return bad;
      return poor;
    case 'sugars':
      if(percent < 25) return excellent;
      if(percent < 50) return good;
      if(percent < 75) return bad;
      return poor;
    case 'carbohydrates':
      return disabled;
    default:
      return null;
  }
}

const getInfos = (title, value) => {
  const percent = getPercent(title, value);
  const color = getColor(title, percent);
  return { percent: percent, color: color };
}

const toList = (product) => {
  const obj = {
    ...placeholder,
    ...product
  };
  const result = [
    header(obj)
  ];
  if(obj && obj.nova) result.push({ nova: obj.nova });
  if(obj && obj.energy) result.push({ title: {left: 'energy', right: `${obj.energy.value}${obj.energy.unit}`} });
  if(obj && obj.bio) result.push({ title: {left: 'bio'} });

  if(obj.nutriments && obj.nutriments.length > 0) {
    result.push({ title: {left: 'nutriments', right: 'Pour 100g'} });
    obj.nutriments.map(({ name, value, unit }) => {
      const infos = getInfos(name, value);
      result.push({ value: { left: name, right: `${value}${unit ? unit : ''}`, ...infos }});
    })
  }

  if(obj.ingredient && obj.ingredient.length > 0) {
    result.push({ title: {left: 'ingredient', right: ''} });
    obj.ingredient.map(({ name, value, unit }) => {
      const cleanName = name.replace(/[_*]/g, '');
      result.push({ value: { left: cleanName, right: value ? `${value}${unit ? unit : ''}` : null }});
    });
  }

  if(obj.allergens && obj.allergens.length > 0) {
    result.push({ title: {left: 'allergens', right: ''} });
    obj.allergens.map(( value ) => {
      const cleanValue = value.replace(/[_*]/g, '');
      result.push({ value: { left: cleanValue }});
    });
  }

  if(obj.additives) result.push({ title: {left: 'additives', right: obj.additives} });
  if(obj.quantity) result.push({ title: {left: 'quantity', right: obj.quantity} });
  if(obj.barcode) result.push({ title: {left: 'barcode', right: obj.barcode} });

  return result;
}

export default {
  convert,
  toList,
  header
}
